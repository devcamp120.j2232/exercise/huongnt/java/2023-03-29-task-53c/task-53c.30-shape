public abstract class Shape {

    //khai báo thuộc tính
    protected String color = "red";
    protected Boolean filled = true;

    //khởi tạo phương thức
    public Shape() {
    }

    public Shape(String color, Boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    // getter and setter
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Boolean getFilled() {
        return filled;
    }

    public void setFilled(Boolean filled) {
        this.filled = filled;
    }

    //phương thức khác
    public abstract double getArea();
    public abstract double getPerimeter();

    //ghi ra console
    @Override
    public String toString() {
        return "Shape [color=" + color + ", filled=" + filled + "]";
    }

    

    
}

