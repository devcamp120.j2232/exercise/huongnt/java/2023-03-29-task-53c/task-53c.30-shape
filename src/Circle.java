public class Circle extends Shape {
    protected double radius = 1.0;

    
//khởi tạo phương thức
    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(String color, Boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        // TODO Auto-generated method stub
        return Math.PI * Math.pow(this.radius,2);
    }

    @Override
    public double getPerimeter() {
        // TODO Auto-generated method stub
        return 2 * Math.PI * radius;
    }

        //ghi ra console
    @Override
    public String toString() {
        return "Circle [Shape [color =" + this.color + ", filled = "+ this.filled + "], radius=" + radius + "]";
    }



    
}
