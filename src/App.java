public class App {
    public static void main(String[] args) throws Exception {
        // khai báo đối tượng Circle
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle("Red", true, 3.0);

        System.out.println("Circle 1");
        System.out.println(circle1.toString());
        System.out.println("Circle 2");
        System.out.println(circle2.toString());
        System.out.println("Circle 3");
        System.out.println(circle3.toString());

        // tính area của circle
        System.out.println("Area of circle 1: " + circle1.getArea());
        System.out.println("Area of circle 2: " + circle2.getArea());
        System.out.println("Area of circle 3: " + circle3.getArea());

        // tính Perimeter của circle
        System.out.println("Perimeter of circle 1: " + circle1.getPerimeter());
        System.out.println("Perimeter of circle 2: " + circle2.getPerimeter());
        System.out.println("Perimeter of circle 3: " + circle3.getPerimeter());

        // khai báo đối tượng rectangle
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(2.5, 1.5);
        Rectangle rectangle3 = new Rectangle("Pink", true, 2.0, 1.5);

        System.out.println("Rectangle 1");
        System.out.println(rectangle1.toString());
        System.out.println("Rectangle 2");
        System.out.println(rectangle2.toString());
        System.out.println("Rectangle 3");
        System.out.println(rectangle3.toString());

        // tính area của rectangle
        System.out.println("Area of Rectangle 1: " + rectangle1.getArea());
        System.out.println("Area of Rectangle 2: " + rectangle2.getArea());
        System.out.println("Area of Rectangle 3: " + rectangle3.getArea());

        // tính Perimeter của rectangle
        System.out.println("Perimeter of Rectangle 1: " + rectangle1.getPerimeter());
        System.out.println("Perimeter of Rectangle 2: " + rectangle2.getPerimeter());
        System.out.println("Perimeter of Rectangle 3: " + rectangle3.getPerimeter());

        // khai báo đối tượng square
        Square square1 = new Square();
        Square square2 = new Square(1.5);
        Square square3 = new Square("Black", true, 1.8);

        System.out.println("Square 1");
        System.out.println(square1.toString());
        System.out.println("Square 2");
        System.out.println(square2.toString());
        System.out.println("Square 3");
        System.out.println(square3.toString());

        // tính area của Square
        System.out.println("Area of square 1: " + square1.getArea());
        System.out.println("Area of square 2: " + square2.getArea());
        System.out.println("Area of square 3: " + square3.getArea());

        // tính Perimeter của Square
        System.out.println("Perimeter of Square 1: " + square1.getPerimeter());
        System.out.println("Perimeter of Square 2: " + square2.getPerimeter());
        System.out.println("Perimeter of Square 3: " + square3.getPerimeter());

    }
}
