public class Square extends Rectangle{
    //protected double side = 1.0;

    //khởi tạo phương thức
    public Square() {
    }

    public Square(double side) {
        this.width = side;
        this.length = side;
    }

    public Square(String color, boolean filled, double side) {
        super.color = color;
        super.filled = filled;
        this.width = side;
        this.length = side;
    }

    //getter and setter

    public void setSide(double side) {
        this.width = side;
    }

    //phương thức khác
    public void setWidth(double side) {
        this.width = side;
    }

    public void setLength(double side) {
        this.length = side;
    }


    @Override
    public double getArea( ) {
        //super.width = this.side;
        //super.length = this.side;
        // TODO Auto-generated method stub
        return super.getArea();
    }

    @Override
    public double getPerimeter() {
        // TODO Auto-generated method stub
        //super.width = this.side;
        //super.length = this.side;
        return super.getPerimeter();
    }
    
    
}
